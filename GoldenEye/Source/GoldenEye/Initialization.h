#include "windows.h"
#include "direct.h"
#include <iostream>
#include<stdio.h>
#include <intrin.h>
#include <string>
//#include <atlstr.h>//Used for CString output

#define LOG_ERROR(x) std::cerr << x << std::endl;
class Init
{
public:
	 bool CheckStorage(const DWORDLONG diskSpaceNeeded);
	 bool IsOnlyInstance(LPCTSTR gameTitle);
	 void Initinstance(LPCTSTR title);
	 bool CheckMemory(const DWORDLONG physicalRAMNeeded, const DWORDLONG virtualRAMNeeded);
	 DWORD ReadCPUSpeed();
	 std::string ReadCPUArch();
};