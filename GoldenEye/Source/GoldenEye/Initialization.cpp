#include "Initialization.h"

void Init::Initinstance(LPCTSTR title)
{
	//std::cout << CheckStorage << std::endl;
	IsOnlyInstance(title);
	CheckStorage(300);
	CheckMemory(1000000, 1000000);
	ReadCPUSpeed();
	//std::cout << ReadCPUArch() << std::endl;
	ReadCPUArch();
	
}

bool Init::CheckStorage(const DWORDLONG diskSpaceNeeded)
{
	// Check for enough free disk space on the current disk.
	int const drive = _getdrive();
	struct _diskfree_t diskfree;
	_getdiskfree(drive, &diskfree);
	unsigned __int64 const neededClusters = (diskSpaceNeeded * 1048576) /
		(diskfree.sectors_per_cluster*diskfree.bytes_per_sector);
	if (diskfree.avail_clusters < neededClusters) {
		// if you get here you don�t have enough disk space!
		//GCC_ERROR(�CheckStorage Failure : Not enough physical	storage.�);
		LOG_ERROR("CheckStorage Failure : Not enough physical storage.");
		return false;
	}
	std::cout << "Sufficient disk space. Only " << diskSpaceNeeded << "MB needed." << std::endl;
	return true;
}

bool Init::IsOnlyInstance(LPCTSTR gameTitle) {
	HANDLE handle = CreateMutex(NULL, TRUE, gameTitle);
	if (GetLastError() != ERROR_SUCCESS) {
		//HWND hWnd = FindWindow(gameTitle, NULL);
		//if (hWnd) {
		/* An instance of your game is already running.
		ShowWindow(hWnd, SW_SHOWNORMAL);
		SetFocus(hWnd);
		SetForegroundWindow(hWnd);
		SetActiveWindow(hWnd);*/
		std::cout << "Another instance of the window is running." << std::endl;
		return false;
		//}

	}
	std::cout << "Single instance of window running." << std::endl;
	return true;
}

bool Init::CheckMemory(const DWORDLONG physicalRAMNeeded, const	DWORDLONG virtualRAMNeeded) {
	MEMORYSTATUSEX status;
	status.dwLength = sizeof(status);
	GlobalMemoryStatusEx(&status);
	if (status.ullTotalPhys < physicalRAMNeeded) {
		/* you don�t have enough physical memory. Tell the
		player to go get a real computer and give this one to
		his mother. */
		LOG_ERROR("CheckMemory Failure : Not enough physical memory.");
		return false;
	}
	else
	{

		std::cout << "Sufficient Physical Memory. Physical Memory Available: " << (status.ullAvailPhys / (float)(1024 * 1024 * 1024)) << " Gigabytes available." << std::endl;
	}
	// Check for enough free memory.
	if (status.ullAvailVirtual < virtualRAMNeeded) {
		// you don�t have enough virtual memory available.
		// Tell the player to shut down the copy of Visual
		//Studio running in the background.
		LOG_ERROR("CheckMemory Failure : Not enough virtual	memory.");
		return false;
	}
	else
	{

		std::cout << "Sufficient Virtual Memory. Virtual Memory Available: " << status.ullAvailVirtual << " Bytes available." << std::endl;

	}
	return true;
}

DWORD Init::ReadCPUSpeed() {
	DWORD BufSize = sizeof(DWORD);
	DWORD dwMHz = 0;
	DWORD type = REG_DWORD;
	HKEY hKey;
	// open the key where the proc speed is hidden:
	long lError = RegOpenKeyEx(HKEY_LOCAL_MACHINE, "HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0", 0, KEY_READ, &hKey);
	if (lError == ERROR_SUCCESS) {
		// query the key:
		RegQueryValueEx(hKey, "~MHz", NULL, &type, (LPBYTE)&dwMHz, &BufSize);
		std::cout << "CPU Speed is \n\t~" << ((float)dwMHz * 0.001f) << "GHz." << std::endl;
	}
	return dwMHz;
}

std::string Init::ReadCPUArch() {
	DWORD BufSize = 1024;
	char name[1024];
	std::string dwMHz;
	DWORD type = REG_SZ;
	HKEY hKey;
	// open the key where the proc speed is hidden:
	long lError = RegOpenKeyEx(HKEY_LOCAL_MACHINE, "HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0", 0, KEY_READ, &hKey);
	if (lError == ERROR_SUCCESS) {
		// query the key:
		RegQueryValueEx(hKey, "ProcessorNameString", NULL, NULL, (LPBYTE)&name, &BufSize);
		//std::cout << "CPU Speed is ~" << dwMHz << "GHz, ";
	}
	std::cout << "Your processor Architecture: \n\t" << name << std::endl;
	return std::string(name);
}






